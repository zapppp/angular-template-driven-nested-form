import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-parent-form',
  templateUrl: './parent-form.component.html',
  styleUrls: ['./parent-form.component.css']
})
export class ParentFormComponent implements OnInit {

  @ViewChild('f')
  form: NgForm;

  constructor() { }

  ngOnInit() {
  }

  send(form: NgForm): void {
    console.log('send(): ', form.value);
  }

  setDefaults(): void {
    this.form.setValue(
      {
        "firstName": "Joku",
        "lastName": "Jamppa",
        "address": {
          "street": "Jossain",
          "house": "123"
        }
    });
  }
}
