import { Component, OnInit } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ] // https://angular.io/guide/dependency-injection
})
export class AddressFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
