import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ParentFormComponent } from './parent-form/parent-form.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { SubFormComponent } from './sub-form/sub-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentFormComponent,
    AddressFormComponent,
    SubFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
